const mongoose = require('mongoose')
const User = require('../models/User')
const { ROLE } = require('../constant')
mongoose.connect('mongodb://localhost:27017/example')
async function clearuser () {
  await User.deleteMany({})
}
async function main () {
  await clearuser()
  const user = new User({ username: 'user@mail.com', password: 'password', roles: [ROLE.User] })
  user.save()
  const admin = new User({ username: 'admin@mail.com', password: ' password', roles: [ROLE.Admin, ROLE.User] })
  admin.save()
}

main().then(function () {
  console.log('finish')
})
