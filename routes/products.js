const express = require('express')
const router = express.Router()
const Product = require('../models/Product')
// const Products = [
//   { id: 1, name: 'IPad gen1 64G wifi', price: 11000.0 },
//   { id: 2, name: 'IPad gen2 64G wifi', price: 12000.0 },
//   { id: 3, name: 'IPad gen3 64G wifi', price: 13000.0 },
//   { id: 4, name: 'IPad gen4 64G wifi', price: 14000.0 },
//   { id: 5, name: 'IPad gen5 64G wifi', price: 15000.0 }
// ]
// const lastId = 6
const getProducts = async function (req, res, next) {
  try {
    const products = await Product.find({}).exec()
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getProduct = async function (req, res, next) {
  const id = req.params.id
  try {
    const product = await Product.findById(id).exec()
    if (product === null) {
      return res.status(404).json({
        message: 'Product not found!!'
      })
    }
    res.json(product)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
  // res.json(Products[index])
}
const addProducts = async function (req, res, next) {
  // const newProduct = {
  //   id: lastId,

  // }
  // Products.push(newProduct)
  // lastId++
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const updateProduct = async function (req, res, next) {
  const productId = req.params.id
  try {
    const product = await Product.findById(productId)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
  // const Product = {
  //   id: productId,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // const index = Products.findIndex(function (item) {
  //   return item.id === productId
  // })
  // if (index >= 0) {
  //   Products[index] = Product
  //   res.json(Products[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No product id ' + req.params.id
  //   })
  // }
}

const deleteProduct = async function (req, res, next) {
  const productId = req.params.id
  try {
    await Product.findByIdAndDelete(productId)
    return res.status(404).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getProducts)// GETall
router.get('/:id', getProduct)// GETid
router.post('/', addProducts)// add
router.put('/:id', updateProduct)// update
router.delete('/:id', deleteProduct)// delete

module.exports = router
